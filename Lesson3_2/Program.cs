﻿/*
 Перетворення стрічок в дані для стандартних типів
 Класс Convert/Сast/TryParse/Parse   explicit convertation (implicit - це неявне)
 */

using System;

namespace Lesson3_2
{
	public enum Procedures : byte
	{
		Add,
		Update,
		Delete,
		Read,
	}

	class Program
	{
		static Lesson4.Test.Class2WithNamespace Test1 { get; set; }
		static void Main(string[] args)
		{
			//Методи класу коверт не надійні, бо може виникнути виключення при конвертації
			Console.WriteLine("Enter integer");
			int val1 = Convert.ToInt32(Console.ReadLine());
			Console.WriteLine("Enter boolean");
			bool val2 = Convert.ToBoolean(Console.ReadLine());
			//etc але тільки для стандартних типів та DateTime і дозволяє перетворювати тільки стрічки

			//В кожного типу є статичний метод Parse що дозволяє приводити стрічки до вказаного типу
			//Ці методи також є ненадійними і потребують додаткової обробки виключень
			bool val3 = bool.Parse("True"); //true
			bool val4 = bool.Parse("False"); //false
			int val5 = int.Parse("15"); //15

			//В кожного стандартного типу також є метод TryParse()

			//по старинке
			bool val6;
			if (bool.TryParse("True", out val6)) //bool.TryParse("True", out val6) поверне true якщо вдалося зробити конвертацію
			{
				//val6 буде заповнене сконвертованим значенням
			}

			//C# 6+
			if (int.TryParse("45", out var val7))
			{
				//val7 буде заповнене сконвертованим значенням
			}

			//Використання каста
			double val8 = 2.5;
			int val9 = (int)val8; //2 Explicit


			string enumString = "Add";

			Procedures procedure = (Procedures)Convert.ToByte(enumString);

			//Захист від виключень швидкодія та четабельність
			if (Enum.TryParse(enumString, out Procedures result))
			{
			}

			var procedure2 = (Procedures)Enum.Parse(typeof(Procedures), enumString);

			Console.WriteLine(val1);
			Console.ReadKey();
		}
	}
}
