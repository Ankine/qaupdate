﻿using Lesson4.SomeNameSpace;
using Lesson4.Test;

namespace Lesson3_2
{
	public class TestClass
	{
		public MyClass MyProperty { get; set; }
		public Class2WithNamespace Test1 { get; set; }
		public Class1WithNamespace Test2 { get; set; }
	}
}
