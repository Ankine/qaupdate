﻿/*
 Тернарний оператор
 Робота з стрічками

 String це ссилочний тип
 При здійсненні операції над звичайною стрічкою завжди створюється новий екземпляр
 Для того щоб можна було редагувати стрічку без створення нового екземпляру використовують StringBuilder
 */

using System;
using System.Globalization;

namespace Lesson3
{
	class Program
	{
		static void Main(string[] args)
		{
			#region Тернарний оператор

			int val1 = 1;
			int val2 = 2;

			string result = val2 > val1 ? "Case1" : "Case2"; //Case1
			string result2 = "Case2" == (val2 > val1 ? "Case1" : "Case2") ? "x" : "y"; //y

			#endregion


			string test1 = "Hello world";
			string upperCase = test1.ToUpper();
			var test = upperCase[3];
			string test2 = test1 + ' ' + test1;

			
			string formatedString = string.Format("Value1 = {0} Value2 = {1}", val1, val2); //Value1 = 1 Value2 = 2
			string interpolatedString = $"Value1 = {val1} Value2 = {val2}";
			string specialCharactersString = "test\\\" \x39";   //test\ '" тута можна писати коди символів ASCII 
			string path = @"C:\user\
буквальна стрічка
може мати
декілька рядків"; //Буквальна строка

			//Прийшла стрічка і треба перевірити чи пуста
			var inputString = " ";

			if (string.IsNullOrEmpty(inputString)) //false
			{
			}
			if (string.IsNullOrWhiteSpace(inputString)) // true
			{
			}

			var array1 = new [] {1, 2, 3, 4};

			var jounResult = string.Join("|", array1); //1|2|3|4
			var jounResult2 = string.Join("-", "first", "second", "last");//first-second-last

			string[] outArray = jounResult.Split('|',' '); //["1","2" etc]
			string[] outArray2 = jounResult2.Split('-'); //["first","second","last"]

			var substringResult = test1.Substring(7); // world 
													  //test1.Substring(7,2); // wo

			var testSTring1 = "     sdfgsdfgsdfg    ";
			var trimstart = testSTring1.TrimStart(); //sdfgsdfgsdfg    
			var trimend = testSTring1.TrimEnd(); //     sdfgsdfgsdfg
			var trim = testSTring1.Trim(); //sdfgsdfgsdfg

			Console.WriteLine(jounResult);
			Console.WriteLine(test1.ToLower().Contains("World".ToLower())); //true
			Console.WriteLine(test2);
			Console.WriteLine(test);
			Console.WriteLine(test1.Length);
			Console.ReadKey();

		}
	}
}
