﻿/*
 Масиви
 Масив - це фіксований набір даних одного типу
 Масиви бувають:

 Одновимірні
 Багатовимірні
 Рвані
 */

using System;

namespace Lesson4_3
{
	class Program
	{
		static void Main(string[] args)
		{
			//Одновимірні масиви
			int[] array0;
			int[] array1 = new int[5];
			var array2 = new int[5];
			var array3 = new int[3] {2, 3, 5};
			var array4 = new int[] { 2, 3, 5 };
			var array5 = new [] { 2, 3, 5 };
			int[] array6 = { 2, 3, 5 };

			var someValue = array6[0];
			array6[0] = 2;

			foreach (var i in array3)
			{
				Console.WriteLine(i);
			}

			//Двовимірні масив масиви
			int[,] x2Arra1 = new int[2,6];
			var x2Array2 = new int[,] {{2, 1}, {2, 5}}; //2*2 array

			x2Array2[1, 2] = 5;
			var someValue2 = x2Array2[1, 2];

			var x2Arra3 = new int[2, 3];
			for (int i = 0; i < 2; i++)
			{
				for (int j = 0; j < 3; j++)
				{
					x2Arra3[i, j] = i * j;
				}
			}

			//Рвані масиви
			//Це по суті масив у масиві

			var test = new int[2][]
			{
				new int[] {5, 5, 2, 4},
				new int[] {5, 5},
				//new int[,] {{ 5, 5 }, { 5, 5 } }
			};

			var test2 = new int[2][,]
			{
				new int[,] { { 5, 5 }, { 5, 5 }, { 5, 5 }, {5, 5}},
				new int[,] { { 5, 5 }, {5, 5}},
				//new int[,] {{ 5, 5 }, { 5, 5 } }
			};

			var test3 = new int[2][][]
			{
				test,test
			};

			//методи масивів

			var myArray = new int[6] {5, 4, 3, 3, 3, 5};

			var generatedArray = Array.CreateInstance(typeof(int), 2, 54);

			var indexOfValue = Array.IndexOf(myArray, 3); //2
			var lastIndexOfValue = Array.LastIndexOf(myArray, 3); //4
		}
	}
}
