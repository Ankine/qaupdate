﻿/*
 Розгалуження
 (Директипи препроцесора) 
 */

using System;

public enum Operation
{
	None,
	Add,
	Remove,
	Update,
	Delete,
	Search
}

namespace Lesson4_2
{
	class Program
	{
		static void Main(string[] args)
		{

#if DEBUG //Release писати неможна бо такої системної нема
			// Цей кусок коду буде компілюватися тільки в Debug режимі
#else
// Цей кусок коду буде компілюватися тільки в Release режимі
#endif
			//If

			if (true)
			{
				//if condition is true
			}

			if (false)
			{
			}
			else
			{
				//if condition false
			}

			if (true)
			{
			}else if (false)
			{
			} //else if може бути багато але небажано


			//switch-case
			var valueForswitch = Operation.Add;

			switch (valueForswitch)
			{
				case Operation.Add:
					{
						Console.WriteLine("Add operation");
						break;
					}
				case Operation.Remove:
					{
						Console.WriteLine("Remove operation");
						break;
					}
				case Operation.Update:
				case Operation.Delete:
				{
					Console.WriteLine("Update or Delete operation");
					break;
				}
				case Operation.Search:
				{
					Console.WriteLine("Update or Delete operation");
					goto case Operation.Add;
				}
				default: Console.WriteLine("None operation"); break;
			}

			var tResult = valueForswitch == Operation.Add ? "Add operation" : "Not Add operation";
		}
	}
}
