﻿/*
 Типи данних
 Зачущі типи () struct enum 
 Ссилочні типи () class delegate array
 */
using System;

namespace Lesson2
{
	class Program
	{
		static void Main(string[] args)
		{
			#region Числові типи

			#region Цілочисельні типи (value types)

			byte myByte = 255;
			//0 => 2^8 -1 C#
			Byte mydByte = 255; // .Net

			sbyte mySbyte = 127; // -2^8/2 => 2^8/2-1
			SByte mydSbyte = -127;

			short myShort = 32767; // -2^16/2 => 2^16/2-1
			Int16 mydShort = 0;

			ushort myUShort = 64000; // 0 => 2^16-1
			UInt16 mydUShort = 0;

			int myInt = 0; // -2^32/2 => 2^32/2-1
			Int32 mydInt = 0;

			uint myuInt = 0; // 0 => 2^32-1
			UInt32 myduInt = 0;

			long myLong = 0; // -2^64/2 => 2^64/2-1
			Int64 mydLong = 0;

			ulong myuLong = 0; // 0 => 2^64-1
			UInt64 myduLong = 0;

			#endregion

			#region Типи з плаваючою комою

			float myFloat = 2.2F;
			double myDouble = 2.2;

			#endregion

			#endregion

			#region Логічні типи

			bool my1Bool = false;
			bool my2Bool = true;

			#endregion

			#region Символьні типи

			char myChar = 'F';
			string myString = "Hello world"; //ссилочний тип

			#endregion

			#region Other (Special)

			DateTime date = DateTime.Now;
			TimeSpan span = new TimeSpan();
			decimal myDec = new decimal(2.555555);

			#endregion

			Console.ReadKey();
		}
	}
}
