﻿/*
 Математичні та логічні оператори
 Приорітети операцій

 Існує декілька типів операторів
	1. Унарний - операція над одною змінною
	2. Бінарний - операція над двома змінними
	3. Тернарний - операція над тробома змінними

	1. Логічні оператори
	2. Математичні оператори
	3. Побітові оператори (розглянути)
	4. Оператори порівняння
	5. Спеціальні
 */

using System;

namespace Lesson2_2
{
	class Program
	{
		public static bool Check1()
		{
			return false;
		}

		public static bool Check2()
		{
			return false;
		}

		static void Main(string[] args)
		{
			#region Матаматичні оператори

			int a = 45;
			int b = 55;

			int c1 = a + b; //100
			int c2 = a - b; //-10
			int c3 = a * b; //2475
			int c4 = a / b; //0 решта відріжеться та відбудуться втрата даних
							//double c4 = (double)a / b; (double) - явне приведення
							//можна зробити змінні а та b тибу double або float
			int c5 = b % a; //10
			int c6 = -b;

			b = 2;
			int c7 = ++b; //Інкремент префіксний //3 b=3
			int c8 = --b; //Інкремент префіксний //2 b=2
			int c9 = b++; //Інкремент суфіксний //2 b=3
			int c10 = b--; //Інкремент суфіксний //3 b=2

			//Існує додаткова бібліотека Math для складныших математичних операцій
			double mySqrt = Math.Sqrt(21454); //147.47

			#endregion

			#region Логічні оператори & Оператори порівняння

			//And
			bool b1 = true & true; //true
			bool b2 = true & false; //false

			//OR
			bool b3 = true | false; //true
			bool b4 = false | false; //false

			//XOR Виключне або. Повертає true у випадку коли значення оперантів різні
			bool b5 = false ^ true; //true

			//NOT
			bool b6 = !b5;

			//& методи Check1 та Check2 будуть викликатися завжди
			if (Check1() & Check2())
			{
			}

			//Якщо результату матоду Check1 достатньо "щоб винести вердикт", то Check2 викликатися небуде
			if (Check1() && Check2())
			{
			}

			//& методи Check1 та Check2 будуть викликатися завжди
			if (Check1() | Check2())
			{
			}

			//Якщо результату матоду Check1 достатньо "щоб винести вердикт", то Check2 викликатися небуде
			if (Check1() || Check2())
			{
			}


			var final = b1 && b2 || b3 && (b4 || b5);

			bool l1 = 10 > 15; //false
			bool l2 = 15 > 10; // true
			bool l3 = 15 >= 15; // true
			bool l4 = 15 <= 10; // false
			bool l5 = 15 == 10; // false
			bool l6 = 15 != 10; // true

			#endregion
		}
	}
}
