﻿/*
 Поннятя областей видимості та простори імен
 */

namespace Lesson4
{
	//namespace може бути в середині іншого неймспейса
	namespace SomeNameSpace
	{
		public class MyClass
		{
		}
	}

	class Program
	{
		//поле класу
		private static string value4 = "7";

		static void Function1()
		{
			//value4 доступне
			var value3 = 8;
		}

		static string Function2()
		{
			var result = "Hello world";
			return result;
		}

		static void Main(string[] args)
		{
			//value4 доступне
			var isValid = false; //локальна змінна
			int value0 = 9;
			if (isValid)
			{
				//value4 доступне
				//value0 доступне бо ця змынна оголошена в базовій області видимості
				int value1 = 10;
			}
			//value1 недоступне

			var newVariable = Function2(); //newVariable ніяк не відноситься до змінної result;
		}
	}
}
